module gitlab.com/joao.p.rodrigo/saprom

go 1.19

require (
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/pelletier/go-toml/v2 v2.0.5 // indirect
	golang.org/x/sys v0.0.0-20190916202348-b4ddaad3f8a3 // indirect
)
