package outlook

import (
	"fmt"

	ole "github.com/go-ole/go-ole"
	"github.com/go-ole/go-ole/oleutil"
	"gitlab.com/joao.p.rodrigo/saprom/email"
)

type OutlookEmail struct {
	outlook *ole.IDispatch
}

func NewOutlookEmailer() (*OutlookEmail, error) {
	ole.CoInitialize(0)
	unknown, err := oleutil.CreateObject("Outlook.Application")
	if err != nil {
		return nil, fmt.Errorf("newOutlookEmailer: %w", err)
	}
	outlook, err := unknown.QueryInterface(ole.IID_IDispatch)
	if err != nil {
		return nil, fmt.Errorf("newOutlookEmailer: %w", err)
	}

	return &OutlookEmail{outlook: outlook}, nil
}

func (o *OutlookEmail) SendEmail(emailData email.Email) error {
	mail, err := oleutil.CallMethod(o.outlook, "CreateItem", 0)
	if err != nil {
		return fmt.Errorf("sendEmail: %w", err)
	}

	mailDispatch := mail.ToIDispatch()
	err = o.fillMailDispatchProps(mailDispatch, emailData)
	if err != nil {
		return fmt.Errorf("sendEmail: %w", err)
	}

	_, err = oleutil.CallMethod(mailDispatch, "send")
	if err != nil {

		return fmt.Errorf("sendEmail: %w", err)
	}

	return nil
}

func (o *OutlookEmail) fillMailDispatchProps(mailDispatch *ole.IDispatch, emailData email.Email) error {
	emailProps := map[string]string{
		"To":       email.AddressesToColonList(emailData.To),
		"Cc":       email.AddressesToColonList(emailData.Cc),
		"Bcc":      email.AddressesToColonList(emailData.Bcc),
		"Subject":  emailData.Subject,
		"HtmlBody": emailData.Body,
	}

	var err error
	for prop, data := range emailProps {
		_, err = oleutil.PutProperty(mailDispatch, prop, data)
		if err != nil {
			return err
		}
	}

	if len(emailData.AttachmentFiles) > 0 {
		attachmentsVariant, err := oleutil.GetProperty(mailDispatch, "Attachments")
		if err != nil {
			return err
		}

		attachmentDispatch := attachmentsVariant.ToIDispatch()

		for _, fp := range emailData.AttachmentFiles {
			_, err = oleutil.CallMethod(attachmentDispatch, "Add", fp)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
