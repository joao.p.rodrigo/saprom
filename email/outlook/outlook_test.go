package outlook

import (
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/joao.p.rodrigo/saprom/email"
)

func TestNewOutlookEmailer(t *testing.T) {
	_, err := NewOutlookEmailer()
	if err != nil {
		t.Fail()
	}
}

func TestMultipleEmailers(t *testing.T) {
	for i := 0; i < 4; i++ {

		_, err := NewOutlookEmailer()
		if err != nil {
			t.Fail()
		}
	}
}

func getUserEmail(t *testing.T) string {
	mailAddress := os.Getenv("TEST_EMAIL")
	if mailAddress == "" {
		t.Fatal("environment TEST_EMAIL must have valid email address for tests")
	}

	return mailAddress
}
func TestSendEmail(t *testing.T) {
	mailData := email.Email{
		To:      []string{getUserEmail(t)},
		Subject: "golang test email",
		Body:    "<h1>This is a test email</h1>",
	}

	mailer, err := NewOutlookEmailer()
	if err != nil {
		t.Fail()
	}

	err = mailer.SendEmail(mailData)
	if err != nil {
		t.Fail()
	}
}

func TestSendEmailWithAttachment(t *testing.T) {
	file, err := ioutil.TempFile("", "attachment.txt")
	if err != nil {
		t.Fatal(err)
	}
	defer os.Remove(file.Name())

	_, err = file.WriteString("This was a test attachment")
	if err != nil {
		t.Fatal(err)
	}

	mailData := email.Email{
		To:              []string{getUserEmail(t)},
		Subject:         "golang test email",
		Body:            "<h1>This is a test email with attachment</h1>",
		AttachmentFiles: []string{file.Name()},
	}

	mailer, err := NewOutlookEmailer()
	if err != nil {
		t.Fatal(err)
	}

	err = mailer.SendEmail(mailData)
	if err != nil {
		t.Fatal(err)
	}
}
