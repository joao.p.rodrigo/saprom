package email

import "strings"

type Email struct {
	To              []string
	Cc              []string
	Bcc             []string
	Subject         string
	Body            string
	AttachmentFiles []string
}

func AddressesToColonList(addresses []string) string {
	return strings.Join(addresses, ";")
}
